import os, random
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import precision_score, accuracy_score, recall_score
from sklearn.naive_bayes import GaussianNB
random.seed(200)
MIN =  np.finfo(np.float64).tiny

#parses csv into two dimensional numpy array
def getData(fp, randomize = True):
    table = []
    with open(fp) as fh:
        for line in fh:
            line = line.strip()
            table.append(line.split(','))
    
    if randomize:
        random.shuffle(table)
    
    return np.array(table, dtype=np.float64)

#split data into training and testing sets
def splitData(table, expIndex = -1):
    #split data into two sets
    half = len(table)/2
    trainData = table[:half]
    testData = table[half:]
    
    #replace 0s with -1 for classifier column
    trainData[trainData[:, -1] == 0, expIndex] = -1
    testData[testData[:, -1] == 0, expIndex] = -1
    
    #seperate into positive and negative examples
    posTrainData = trainData[trainData[:, expIndex] == 1] 
    negTrainData = trainData[trainData[:, expIndex] == -1] 
    
    #get column that has the classification
    testExp = testData[:,expIndex]
    trainExp = trainData[:,expIndex]
    
    #remove column that has classification. Only features are left
    testData = np.delete(testData, expIndex, axis=1)
    trainData = np.delete(trainData, expIndex, axis=1)
    posTrainData = np.delete(posTrainData, expIndex, axis=1)
    negTrainData = np.delete(negTrainData, expIndex, axis=1)
    
    return testData, testExp, trainData, trainExp, posTrainData, negTrainData

def gaussianFunc(x, mu, sig):
    ratio = np.exp(-np.power((x - mu)/sig, 2.)/2.)
    return (1./(np.sqrt(2.*np.pi)*sig))* ratio
#def gaussianFunc(x, m, s):
#    return (1/(np.sqrt(2*np.pi*s**2)))*np.e**(-0.5*(((x-m)**2)/(s**2)))

def printEval(testExp, predicted):
    print("Accuracy: " + str(accuracy_score(testExp, predicted)))
    print("Precision: " + str(precision_score(testExp, predicted)))
    print("Recall: " + str(recall_score(testExp,predicted)))

    confMatrix = [[0,0],[0,0]]
    for exp, pre in zip(testExp, predicted):
        if exp == -1:
            exp = 0
        if pre == -1:
            pre = 0
        confMatrix[int(exp)][int(pre)] += 1

    print(confMatrix)
    print("")

if __name__ == "__main__":
    #data file is in same folder as script
    dataPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "spambase.data")
    allData = getData(dataPath)

    testFeatures, testExp, trainFeatures, trainExp, posTrainF, negTrainF= splitData(allData)
    numExamples = float(len(posTrainF) + len(negTrainF))

    pPos = len(posTrainF)/numExamples
    pNeg = len(negTrainF)/numExamples
    print(pPos)
    print(pNeg)

    #compute mean and standard deviation per class per column
    meanFPos = posTrainF.mean(axis=0)
    meanFNeg = negTrainF.mean(axis=0)
    stdFPos = posTrainF.std(axis=0)
    stdFNeg = negTrainF.std(axis=0)

    #to avoid divide by 0, replace 0 with smallest float
    stdFPos[stdFPos == 0] = MIN
    stdFNeg[stdFNeg == 0] = MIN

    #test Naieve Bays algorithm
    predicted = []
    for features, exp in zip(testFeatures, testExp):
        probPos = np.log(gaussianFunc(features, meanFPos, stdFPos))
        probPos = np.sum(probPos) + np.log(pPos)

        probNeg = np.log(gaussianFunc(features, meanFNeg, stdFNeg))
        probNeg = np.sum(probNeg) + np.log(pNeg)

        #find stronger classification, break ties at random
        guess = None
        if probPos > probNeg:
            guess = 1
        elif probNeg > probPos:
            guess = -1
        else:
            guess = random.choice([-1, 1])
        
        predicted.append(guess)
    
    printEval(testExp, predicted)


    #to confirm results, also use Baive bayes library
    gnb = GaussianNB()
    gnb.fit(trainFeatures, trainExp)
    predicted = gnb.predict(testFeatures)
    printEval(testExp, predicted)

    #Use logistic regression on save training and test data
    clr = LogisticRegression()
    clr.fit(trainFeatures, trainExp)
    predicted = clr.predict(testFeatures)
    printEval(testExp, predicted)

    print(stdFNeg)
    print(stdFPos)
    print(gnb.theta_)

